#!/bin/bash
# Author:  itknight <598940445@qq.com>
# SPECIAL THANKS: Oneinstack
#
# Notes: Installation of Nginx PHP for CentOS/RedHat 7+ 
#
# Oneinstack Project home page:
#       https://oneinstack.com
#       https://github.com/oneinstack/oneinstack


export PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin
clear
printf "
#######################################################################
#       Installation of Nginx PHP for CentOS/RedHat 7+                #
#                                   itknight, 2023.11                 #
#######################################################################
"
[ $(id -u) != "0" ] && { echo "${CFAILURE}Error: You must be root to run this script${CEND}"; exit 1; }


drzlnp_dir=$(dirname "`readlink -f $0`")

echo $drzlnp_dir
pushd ${drzlnp_dir} > /dev/null
. ./versions.txt
. ./options.conf
. ./include/color.sh
. ./include/check_os.sh
. ./include/check_dir.sh
. ./include/download.sh
. ./include/get_char.sh


ARG_NUM=$#

